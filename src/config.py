from src.base_config import Config


webinar_config = Config(
    correlations={
        'height_to_hair': ('Длина волос (в см):', 'Рост (в см):'),
        'height_to_experience': ('Стаж вождения (в годах):', 'Рост (в см):'),
    },
    nlp_clustering='Твоя фамилия и имя:',
    main_path='data/main_dataset.csv',
    main_page='index.html',
    nlp_page='nlp_analysis.html',
    analys_page='analys.html',
)
