import io
import typing as tp

import numpy as np
import pandas as pd
import spacy
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from scipy.stats import spearmanr
from sklearn.cluster import KMeans


def normalize(vector: np.ndarray) -> np.ndarray:
    return (vector - np.mean(vector)) / np.std(vector)


def get_correlations(df: pd.DataFrame, columns: tp.Tuple[str, str]) -> io.BytesIO:
    # first_column_values = df[columns[0]].values
    # second_column_values = df[columns[1]].values

    first_column_values = normalize(df[columns[0]].values)
    second_column_values = normalize(df[columns[1]].values)

    corr_coeff, p_value = spearmanr(first_column_values, second_column_values)

    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)
    axis.plot(first_column_values)
    axis.plot(second_column_values)
    axis.legend([columns[0], columns[1]])
    axis.set_title(
        f'Совместный граффик - {columns[0]}, {columns[1]}, \n'
        f'Коэффицент корреляции: {round(corr_coeff, 2)} ({round(p_value, 8)})'
    )

    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return output


def nlp_analysis(
        df: pd.DataFrame,
        column_name: str,
        model_path: str,
        n_clusters: int,
) -> pd.DataFrame:
    nlp = spacy.load(model_path)
    if any(df[column_name].isna()):
        df[column_name].fillna('empty', inplace=True)
    names = df[column_name].values
    names_vectors = np.array([nlp(name).vector for name in names])

    cluster_model = KMeans(n_clusters=n_clusters)
    cluster_model.fit(names_vectors)
    counter = dict(zip(*np.unique(cluster_model.labels_, return_counts=True)))

    data = [np.zeros(max(counter.values())).astype(str) for _ in range(n_clusters)]
    for class_ in range(0, n_clusters):
        data[class_][:counter[class_]] = names[cluster_model.labels_ == class_]
    data = np.array(data)

    cluster_df = pd.DataFrame(columns=[f'cluster_{i + 1}' for i in range(n_clusters)], data=data.T)

    return cluster_df
