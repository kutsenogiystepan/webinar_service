from dataclasses import dataclass
import typing as tp


@dataclass
class Config:
    correlations: tp.Dict
    nlp_clustering: str
    main_path: str
    main_page: str
    analys_page: str
    nlp_page: str
