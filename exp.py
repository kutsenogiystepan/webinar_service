from datetime import datetime

import numpy as np
from tqdm import tqdm


def my_sum(my_list):
    total = 0
    for num in my_list:
        total += num
    return total


if __name__ == '__main__':
    number_list = list(range(100000))
    my_times = []
    python_times = []
    for _ in tqdm(range(10000)):
        start = datetime.now()
        _ = my_sum(number_list)
        my_times.append(datetime.now() - start)

        start = datetime.now()
        _ = sum(number_list)
        python_times.append(datetime.now() - start)

    print(np.mean(my_times) / np.mean(python_times))
