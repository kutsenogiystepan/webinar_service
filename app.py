import pandas as pd
from flask import Flask, render_template, request, Response

from src.config import webinar_config
from src.utils import get_correlations, nlp_analysis

app = Flask(
    __name__,
    static_folder='web/static',
    template_folder='web/templates',
)

PORT = 8000
HOST = '0.0.0.0'


@app.route('/', methods=['GET'])
def main_page():
    return render_template(webinar_config.main_page)


@app.route('/plot.png')
def plot_png():
    df = pd.read_csv(webinar_config.main_path)
    output = get_correlations(df, webinar_config.correlations[request.args.get('type')])
    return Response(output.getvalue(), mimetype='image/png')


@app.route('/analys', methods=['GET'])
def put_image():
    return render_template(webinar_config.analys_page, type=request.args.get('type'))


@app.route('/nlp', methods=['GET'])
def nlp():
    df = pd.read_csv(webinar_config.main_path)
    data = nlp_analysis(
        df,
        column_name=webinar_config.nlp_clustering,
        model_path='ru_core_news_sm',
        n_clusters=3,
    )
    return render_template(webinar_config.nlp_page, data=data.to_html())


if __name__ == '__main__':
    app.run(host=HOST, port=PORT, debug=True)
